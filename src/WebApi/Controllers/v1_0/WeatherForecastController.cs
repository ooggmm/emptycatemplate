﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OGM.Template.Empty.Application.WeatherForecasts.Queries.GetWeatherForecasts;
using OGM.Template.Empty.WebApi.Controllers.Common;

namespace OGM.Template.Empty.WebApi.Controllers.v1_0
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ApiController
    {

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult> Get()
        {
            var result = await Mediator.Send(new GetWeatherForecastsQuery());
            return Ok(result);
        }
    }
}
