﻿using OGM.Template.Empty.Application.Common.Interfaces;

namespace OGM.Template.Empty.WebApi.Services
{
    public class CurrentUserService : ICurrentUserService
    {
        public CurrentUserService()
        {
            UserId = "System";
        }

        public string UserId { get; }
    }
}
