﻿using OGM.Template.Empty.Application.Common.Interfaces;
using OGM.Template.Empty.Infrastructure.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OGM.Template.Empty.Infrastructure.Persistence.Contexts;
using OGM.Template.Empty.Application.Common.Interfaces.Persistance;

namespace OGM.Template.Empty.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {

            services.AddDbContext<ApplicationDbContext>(options =>
                  options.UseSqlServer(
                      configuration.GetConnectionString("DefaultConnection"),
                      b => b.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName).CommandTimeout(120)));

            services.AddScoped<IApplicationDbContext>(provider => provider.GetService<ApplicationDbContext>());
            
            services.AddTransient<IDateTime, DateTimeService>();


            return services;
        }
    }
}
