﻿using OGM.Template.Empty.Application.Common.Interfaces;
using System;

namespace OGM.Template.Empty.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
