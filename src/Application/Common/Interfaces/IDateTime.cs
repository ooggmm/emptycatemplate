﻿using System;

namespace OGM.Template.Empty.Application.Common.Interfaces
{
    public interface IDateTime
    {
        DateTime Now { get; }
    }
}
